package com.example.demo.model;

public class Relation {

    private String source;
    private String target;
    private Double weight;

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public Double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "source='" + source + '\'' +
                ", target='" + target + '\'' +
                ", weight=" + weight +
                '}';
    }

    public Relation(String source, String target, Double weight) {
        this.source = source;
        this.target = target;
        this.weight = weight;
    }
}
