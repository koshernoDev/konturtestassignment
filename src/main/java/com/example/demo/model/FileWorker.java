package com.example.demo.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileWorker {

    static public String readFile(String filename) {

        try {
            String content = Files.readString(Path.of(filename), StandardCharsets.UTF_8);

            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
