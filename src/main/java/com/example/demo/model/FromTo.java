package com.example.demo.model;

public class FromTo {

    private String from;
    private String to;

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public FromTo(String from, String to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public String toString() {
        return "FromTo{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
