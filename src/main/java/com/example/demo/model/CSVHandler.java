package com.example.demo.model;

public class CSVHandler {

    public static Relation[] getRelationsFromInput(String csvs) {

        String[] lines = csvs.split("\n");
        Relation[] relations = new Relation[lines.length];

        for (int i = 0; i < lines.length; ++i) {
            String[] values = lines[i].split(",");

            String source = values[0].trim();
            String target = values[1].trim();
            Double weight = Double.parseDouble(values[2].trim());

            relations[i] = new Relation(source, target, weight);
        }

        return relations;
    }
}
