package com.example.demo.model;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Service
public class ConverterService {

    private DirectedWeightedMultigraph<Object, DefaultWeightedEdge> graph;

    @Autowired
    public ConverterService(ApplicationArguments arguments) {

        String path = arguments.getSourceArgs()[0];

        String input = FileWorker.readFile(path);

        Relation[] relations = CSVHandler.getRelationsFromInput(input);

        this.graph = new DirectedWeightedMultigraph<>(DefaultWeightedEdge.class);

        for (Relation relation : relations) {
            this.graph.addVertex(relation.getSource());
            this.graph.addVertex(relation.getTarget());

            DefaultWeightedEdge forwardEdge = this.graph.addEdge(relation.getSource(), relation.getTarget());
            this.graph.setEdgeWeight(forwardEdge, relation.getWeight());

            DefaultWeightedEdge reverseEdge = this.graph.addEdge(relation.getTarget(), relation.getSource());
            this.graph.setEdgeWeight(reverseEdge, 1.0 / relation.getWeight());
        }
    }

    public String convert(FromTo fromTo) {

        String from = fromTo.getFrom();
        String to = fromTo.getTo();

        List<String> fromFractions = List.of(from.split("/"));
        List<String> toFractions = List.of(to.split("/"));

        List<String> fromUppers = new LinkedList<>();
        List<String> toUppers = new LinkedList<>();
        List<String> fromLowers = new LinkedList<>();
        List<String> toLowers = new LinkedList<>();

        splitFromAndToExpressionsIntoFractionsAndCollectTheirUnits(fromFractions, fromUppers, fromLowers);
        splitFromAndToExpressionsIntoFractionsAndCollectTheirUnits(toFractions, toUppers, toLowers);

        // * Replacing denominators to numerators of fractions
        fromUppers.addAll(toLowers);
        toUppers.addAll(fromLowers);

        BigDecimal totalWeight = new BigDecimal(1.0);

        for (String fromUnit : fromUppers) {
            for (String toUnit : toUppers) {
                DijkstraShortestPath<Object, DefaultWeightedEdge> dijkstraAlg
                        = new DijkstraShortestPath<>(this.graph);

                ShortestPathAlgorithm.SingleSourcePaths<Object, DefaultWeightedEdge> paths
                        = dijkstraAlg.getPaths(fromUnit);

                GraphPath<Object, DefaultWeightedEdge> path = paths.getPath(toUnit);

                if (path == null) {
                    continue;
                }

                List<DefaultWeightedEdge> edges = path.getEdgeList();

                for (DefaultWeightedEdge edge : edges) {
                    Double weight = graph.getEdgeWeight(edge);
                    totalWeight = totalWeight.multiply(new BigDecimal(weight));
                }
            }
        }

        String formattedTotalWeight = String.format("%.15f", totalWeight);

        System.out.println("formattedTotalWeight = " + formattedTotalWeight);

        return formattedTotalWeight;
    }

    private void splitFromAndToExpressionsIntoFractionsAndCollectTheirUnits(
            List<String> fractions,
            List<String> uppers,
            List<String> lowers) {

        if (fractions.size() == 2 && fractions.get(0).contains("1")) {
            collectFractionUnits(lowers, fractions.get(1));
        } else if (fractions.size() == 2) {
            collectFractionUnits(uppers, fractions.get(0));
            collectFractionUnits(lowers, fractions.get(1));
        } else if (fractions.size() == 1) {
            collectFractionUnits(uppers, fractions.get(0));
        }
    }

    private void collectFractionUnits(List<String> collection, String fraction) {
        List.of(fraction.split("[*]"))
                .stream()
                .map(unit -> unit.trim())
                .forEach(unit -> collection.add(unit));
    }
}
