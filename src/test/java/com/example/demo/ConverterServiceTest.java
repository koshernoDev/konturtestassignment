package com.example.demo;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.junit.jupiter.api.Test;

public class ConverterServiceTest {

    @Test
    public void jgraphtExample() {
        Graph<String, DefaultWeightedEdge> g = new DirectedWeightedMultigraph<>(DefaultWeightedEdge.class);

        g.addVertex("м");
        g.addVertex("см");
        g.addVertex("мм");
        g.addVertex("км");

        g.addEdge("м", "см", new DefaultWeightedEdge());
        g.setEdgeWeight(g.getEdge("м", "см"), 100);
    }

    @Test
    public void jgraphtGetPathWeightIfExists() {
        DirectedWeightedMultigraph<Object, DefaultWeightedEdge> dwm =
                DirectedWeightedMultigraph
                        .createBuilder(DefaultWeightedEdge.class)
                        .addEdge("м", "см")
                        .addEdge("см", "м")
                        .addEdge("мм", "м")
                        .addEdge("м", "мм")
                        .addEdge("км", "м")
                        .addEdge("м", "км")
                        .build();

        dwm.setEdgeWeight(dwm.getEdge("м", "см"), 100);
        dwm.setEdgeWeight(dwm.getEdge("см", "м"), 0.01);

        dwm.setEdgeWeight(dwm.getEdge("мм", "м"), 0.001);
        dwm.setEdgeWeight(dwm.getEdge("м", "мм"), 1000);

        dwm.setEdgeWeight(dwm.getEdge("км", "м"), 1000);
        dwm.setEdgeWeight(dwm.getEdge("м", "км"), 0.001);

        System.out.println("dwm = " + dwm);

        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"м\", \"см\")) = " + dwm.getEdgeWeight(dwm.getEdge("м", "см")));
        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"см\", \"м\")) = " + dwm.getEdgeWeight(dwm.getEdge("см", "м")));

        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"мм\", \"м\")) = " + dwm.getEdgeWeight(dwm.getEdge("мм", "м")));
        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"м\", \"мм\")) = " + dwm.getEdgeWeight(dwm.getEdge("м", "мм")));

        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"км\", \"м\")) = " + dwm.getEdgeWeight(dwm.getEdge("км", "м")));
        System.out.println("dwm.getEdgeWeight(dwm.getEdge(\"м\", \"км\")) = " + dwm.getEdgeWeight(dwm.getEdge("м", "км")));

        System.out.println("Path from мм to км");

        DijkstraShortestPath<Object, DefaultWeightedEdge> dijkstraAlg = new DijkstraShortestPath<>(dwm);
        ShortestPathAlgorithm.SingleSourcePaths<Object, DefaultWeightedEdge> paths = dijkstraAlg.getPaths("мм");
        GraphPath<Object, DefaultWeightedEdge> path = paths.getPath("км");

        System.out.println("path = " + path);

        final Double[] pathWeight = {1.0};

        path.getEdgeList().stream().forEach(edge -> {
            System.out.println("edge = " + edge);

            String source = (String) dwm.getEdgeSource(edge);
            String target = (String) dwm.getEdgeTarget(edge);

            System.out.println("source = " + source);
            System.out.println("target = " + target);

            Double weight = dijkstraAlg.getPathWeight(source, target);

            System.out.println("weight = " + weight);

            pathWeight[0] *= weight;
        });

        System.out.println("pathWeight[0] = " + pathWeight[0]);
    }
}
