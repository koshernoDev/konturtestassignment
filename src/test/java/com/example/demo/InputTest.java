package com.example.demo;

import com.example.demo.model.CSVHandler;
import com.example.demo.model.FileWorker;
import com.example.demo.model.Relation;
import org.junit.jupiter.api.Test;

public class InputTest {

    @Test
    public void shouldReadInputFileAndGetData() {

        String path = "src/main/resources/input.csv";

        String input = FileWorker.readFile(path);

        Relation[] relations = CSVHandler.getRelationsFromInput(input);

        for (Relation relation : relations) {
            System.out.println("relation = " + relation);
        }
    }
}
